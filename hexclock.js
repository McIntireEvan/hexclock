function doUpdate(text, background) 
{
	setBackground(background)
	setText(text)
}

function setBackground(background) {
	var c = calcHexValue();

	$(background).css("background-color", c)
	
	setTimeout(function() {
		setBackground(background)
		}, 1000)
}

function setText(text) {
	var c = calcHexValue();

	$(text).html(c);
	
	setTimeout(function() {
		setText(text)
		}, 1000)
}

function calcHexValue() {
	var d = new Date();
				
	var h = d.getHours()
	if(h<=9) {
		h = "0" + h;
	}
				
	var m = d.getMinutes()
	if(m<=9) {
		m = "0" + m;
	}
				
	var s = d.getSeconds()
	if(s<=9) {
		s = "0" + s;
	}
				
	var c = "#" + h + m + s;
	return c
}